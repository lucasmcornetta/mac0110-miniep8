function troca(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end

function insercao(v)
    tam = length(v)
    for i in 2:tam
        j = i
        while j > 1
            if compareByValueAndSuit(v[j],v[j - 1])
                troca(v, j, j - 1)
            else
                break
            end
            j = j - 1
        end
    end
    return v
end

function compareByValue(x,y)
    if x[1] == '1'
        aux_x = 10
    elseif x[1] == 'J'
        aux_x = 11
    elseif x[1] == 'Q'
        aux_x = 12
    elseif x[1] == 'K'
        aux_x = 13
    elseif x[1] == 'A'
        aux_x = 14
    else
        aux_x = parse(Int32,x[1])
    end
    if y[1] == '1'
        aux_y = 10
    elseif y[1] == 'J'
        aux_y = 11
    elseif y[1] == 'Q'
        aux_y = 12
    elseif y[1] == 'K'
        aux_y = 13
    elseif y[1] == 'A'
        aux_y = 14
    else
        aux_y = parse(Int32,y[1])
    end
    return aux_x < aux_y
end

function compareByValueAndSuit(x,y)
    if x[length(x)] == '♦'
        suit_x = 1
    elseif x[length(x)] == '♠'
        suit_x = 2
    elseif x[length(x)] == '♥'
        suit_x = 3
    elseif x[length(x)] == '♣'
        suit_x = 4
    end
    if y[length(y)] == '♦'
        suit_y = 1
    elseif y[length(y)] == '♠'
        suit_y = 2
    elseif y[length(y)] == '♥'
        suit_y = 3
    elseif y[length(y)] == '♣'
        suit_y = 4
    end
    if suit_x == suit_y
	return compareByValue(x,y)
    else
    	return suit_x < suit_y
    end
end

using Test

function test_parte1()
    @test compareByValue("2♠", "A♠")
    @test !compareByValue("K♥", "10♥")
    @test !compareByValue("10♠", "10♥")
    println("Fim dos teste (Parte1)")
end

function test_parte2()
    @test compareByValueAndSuit("2♠", "A♠")
    @test !compareByValueAndSuit("K♥", "10♥")
    @test compareByValueAndSuit("10♠", "10♥")
    @test compareByValueAndSuit("A♠", "2♥")
    println("Fim dos teste (Parte2)")
end

